const http = require('http')
const url = require('url')
const users = require('./users.json')

const PORT = process.env.PORT || 3000

const server = http.createServer((request, response) => {
    let url = request.url
    console.log('URL:', url)
    let method = request.method

    switch (method) {
        case "GET":
            if (url === "/api") {
                response.writeHead(200, {"Content-Type": "application/json"})
                response.write(JSON.stringify({message: "Hello World"}))
                response.end()
            } else if (url === "/api/getUsers") {
                response.writeHead(200, {"Content-Type": "application/json"})
                response.write(JSON.stringify({users: server.getUsers()}))
                response.end()
            } else if (url.split("?")[0] === '/api/getUserByName') {
                let idQuery = url.split("?")[1]
                let name = idQuery.split("=")[1]
                response.writeHead(200, {"Content-Type": "application/json"})
                response.write(JSON.stringify({user: server.getUserByName(name)}))
                response.end()
            } else {
                response.writeHead(200, {"Content-Type": "application/json"})
                response.write(JSON.stringify("Endpoint not found"))
                response.end()
            }
            break
        case "POST":
            if (url === "/api/updateUserByName") {
                response.writeHead(200, {"Content-Type": "application/json"})
                response.write(JSON.stringify({user: server.updateUserByName(name)}))
                response.end()
            }
            break
        default:
            response.writeHead(200, {"Content-Type": "application/json"})
            response.write(JSON.stringify("Url Not Found"))
            response.end()
    }
})

server.listen(PORT, () => {
    console.log(`Server has been started on ${PORT}...`)
})

server.getUsers = () => {
    return users
}

server.getUserByName = userName => {
    return users.find(user => user.name === userName)
}

server.updateUserByName = userName => {
    return users.find(user => user.name === userName)
}
